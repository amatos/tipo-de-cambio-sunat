# Tipo de Cambio - Sunat

Este repositorio obtiene el tipo de cambio oficial de la SUNAT para la fecha ingresada. Utiliza `Javalin` para exponer los endpoints.

## Uso

* `mvn clean install`
* `java -jar target/*.jar`

Ingresa a `http://localhost:7000/2018/12/31` para ver el tipo de cambio para esa fecha. Si deseas ver el cambio de todo el mes, ingresa a `http://localhost:7000/2018/12`. En caso de ingresar una fecha (día, mes, año) que no tiene un registro, se buscará la inmediata anterior.
