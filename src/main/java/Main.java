import com.fasterxml.jackson.databind.ObjectMapper;
import io.javalin.Javalin;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    final static String YEAR_MONTH_DAY_FORMAT = "yyyy/MM/dd";
    final static String URL = "http://www.sunat.gob.pe/cl-at-ittipcam/tcS01Alias?mes=";

    public static void main(String[] args) {
        Calendar firstDay = new GregorianCalendar(1995, Calendar.JANUARY, 1);
        ObjectMapper mapper = new ObjectMapper();

        Javalin app = Javalin.create().start(7000);

        app.get("/", ctx -> ctx.result("Uso: \"/year/month/day\" (el dia es opcional.)"));
        app.get("/:param", ctx -> {
            String p = ctx.pathParamMap().get("param");
            ctx.result(p);
        });
        app.get("/:year/:month/:day", ctx -> {
            String yearAsString = ctx.pathParamMap().get("year");
            String monthAsString = ctx.pathParamMap().get("month");
            String dayAsString = ctx.pathParamMap().get("day");
            try {
                int month = Integer.parseInt(monthAsString);
                int day = Integer.parseInt(dayAsString);
                int year = Integer.parseInt(yearAsString);
                String date = year + "/" + month + "/" + day;
                DateFormat df = new SimpleDateFormat(YEAR_MONTH_DAY_FORMAT);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(df.parse(date));
                if (calendar.before(firstDay)) {
                    ctx.result("La fecha ingresada es anterior al primer registro");
                }
                ChangeRate changeRate = null;
                while (changeRate == null) {
                    changeRate = getChangeRateForFullDate(year, month, day);
                    calendar.add(Calendar.DAY_OF_MONTH, -1);
                    year = calendar.get(Calendar.YEAR);
                    month = calendar.get(Calendar.MONTH) + 1;
                    day = calendar.get(Calendar.DAY_OF_MONTH);
                }
                String jsonInString = mapper.writeValueAsString(changeRate);
                ctx.result(jsonInString);
            } catch (NumberFormatException ex) {
                ctx.result("La fecha ingresada o el formato utilizado no es correcto");
            }
        });
        app.get("/:year/:month", ctx -> {
            List<ChangeRate> changeRates;
            String yearAsString = ctx.pathParamMap().get("year");
            String monthAsString = ctx.pathParamMap().get("month");
            try {
                int year = Integer.parseInt(yearAsString);
                int month = Integer.parseInt(monthAsString);
                if (month > 1 || month < 12) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);
                    System.out.println(calendar.toString());
                    changeRates = getRateForGivenMonthAndYear(year, month);
                    String jsonInString = mapper.writeValueAsString(changeRates);
                    ctx.result(jsonInString);
                } else {
                    throw new NumberFormatException();
                }
            } catch (NumberFormatException ex) {
                ctx.result("La fecha ingresada o el formato utilizado no es correcto");
            }
        });
    }

    private static List<ChangeRate> getRateForGivenMonthAndYear(int year, int month) {
        Document doc;
        List<ChangeRate> changeRates = new ArrayList<>();
        try {
            String url = URL + month + "&anho=" + year;
            doc = Jsoup.connect(url).get();
            // Extracting the whole table
            Elements rows = doc.select("table");
            boolean exit = false;
            ChangeRate changeRate = new ChangeRate();
            int counter = 1;

            // 2 so we can ignore the first one containing "Tipo de cambio publicado al :"
            for (int i = 1; i < rows.select("tr").size(); i++) {
                for (int j = 0; j < rows.get(i).select("td").size(); j++) {
                    // while the content is "día", "compra" or "venta" we pass to the next element.
                    while (rows.get(i).select("td").get(j).text().contains("Día")
                            || rows.get(i).select("td").get(j).text().contains("Compra")
                            || rows.get(i).select("td").get(j).text().contains("Venta")) {
                        j++;
                    }

                    // if it contains "Notas" it means that there is no more information
                    // to work with so we brake the first loop (saving the exit boolean for later)
                    if (rows.get(i).select("td").get(j).text().contains("Notas")) {
                        exit = true;
                        break;
                    }

                    /**
                     * Each element <b>always</b> consists of 3 items, the date, the buying price and the sell price
                     * in that specific order!
                     * in case it's the first element we just store the value in its container and so on.
                     */
                    if (counter == 1) {
                        getDateForChangeRate(year, month, rows, changeRate, i, j);
                        counter++;
                    } else if (counter == 2) {
                        // otherwise it breaks...
                        String compra = rows.get(i).select("td").get(j).text();
                        changeRate.setBuy(new BigDecimal(compra));
                        counter++;
                    } else if (counter == 3) {
                        String venta = rows.get(i).select("td").get(j).text();
                        changeRate.setSell(new BigDecimal(venta));
                        changeRates.add(changeRate);

                        // done, we clean the variable and start over.
                        counter = 1;
                        changeRate = new ChangeRate();
                    }
                }

                // if "notas" is present, then the exit boolean has been set to true
                if (exit) {
                    break;
                }
            }
        } catch (IOException ex) {
            return new ArrayList<>();
        }
        return changeRates;

    }

    /**
     * @param year  the year you want to get the information from
     * @param month the month you want to get the information from
     * @param day   the day you want to get the information from
     * @return ChangeRate
     */
    private static ChangeRate getChangeRateForFullDate(int year, int month, int day) {
        Document doc;
        try {
            doc = Jsoup.connect(URL + month + "&anho=" + year).get();
            // Extracting the whole table
            Elements rows = doc.select("table");
            boolean exit = false;
            ChangeRate changeRate;
            // 2 so we can ignore the first one containing "Tipo de cambio publicado al :"
            for (int i = 1; i < rows.select("tr").size(); i++) {
                for (int j = 0; j < rows.get(i).select("td").size(); j++) {
                    // while the content is "día", "compra" or "venta" we pass to the next element.
                    while (rows.get(i).select("td").get(j).text().contains("Día")
                            || rows.get(i).select("td").get(j).text().contains("Compra")
                            || rows.get(i).select("td").get(j).text().contains("Venta")) {
                        j++;
                    }

                    // if it contains "Notas" it means that there is no more information
                    // to work with so we brake the first loop (saving the exit boolean for later)
                    if (rows.get(i).select("td").get(j).text().contains("Notas")) {
                        exit = true;
                        break;
                    }
                    changeRate = getChangeRate(year, month, day, rows, i, j);
                    if (changeRate != null) {
                        return changeRate;
                    }
                }

                // if "notas" is present, then the exit boolean has been set to true
                if (exit) {
                    break;
                }
            }
            return null;
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Each element <b>always</b> consists of 3 items, the date, the buying price and the sell price
     * in that specific order!
     * in case it's the first element we just store the value in its container and so on.
     */
    private static ChangeRate getChangeRate(int year, int month, int day, Elements rows, int i, int j) {
        ChangeRate changeRate = new ChangeRate();
        String tableDay = rows.get(i).select("td").get(j).text();
        if (tableDay.equals(String.valueOf(day))) {
            getDateForChangeRate(year, month, rows, changeRate, i, j);
            String buy = rows.get(i).select("td").get(j + 1).text();
            changeRate.setBuy(new BigDecimal(buy));
            String sell = rows.get(i).select("td").get(j + 2).text();
            changeRate.setSell(new BigDecimal(sell));
            return changeRate;
        }
        return null;
    }

    private static void getDateForChangeRate(int year, int month, Elements rows, ChangeRate changeRate, int i, int j) {
        int documentDay = Integer.parseInt(rows.get(i).select("td").get(j).text());
        changeRate.setDate(year + "/" + (month < 10 ? "0" + month : month) + "/" + (documentDay < 10 ? "0" + documentDay : documentDay));
    }


    private static class ChangeRate {
        private String date;
        private BigDecimal buy;
        private BigDecimal sell;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public BigDecimal getBuy() {
            return buy;
        }

        public void setBuy(BigDecimal buy) {
            this.buy = buy;
        }

        public BigDecimal getSell() {
            return sell;
        }

        public void setSell(BigDecimal sell) {
            this.sell = sell;
        }
    }
}
